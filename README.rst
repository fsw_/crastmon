crastmon
==========

This was developed for personal use and is not in a 'working' stage!

"crastmon" is a simple tool to monitor time spend on work and procrastination.

It works on Linux and uses "xprop" and "xprintidle" tools.

"crastmond" is a daemon that should run in background (check scripts in install to put it in init.d). 
Each few seconds it is saving title of an active window to database.

"crastmon" is a command-line tool. 
It can define simple set of regexps and generate statistics for given perion of time.

Example
==========

::
    
    $ crastmon add "work" "^.*Eclipse\ SDK$"
    $ crastmon add "play" "^Xonotic$"
    $ crastmon rules
    1) "work" /^.*Eclipse\ SDK$/
    2) "play" /^Xonotic$/
    $ crastmon
    total active time: 1h 0m 0s
    idle time: 25m 0s
    work: 30m 0s (50%)
    play: 15m 0s (25%)
    ?: 15m 0s (25%)


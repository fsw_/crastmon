try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name="crastmon",
    version="0.5.0",
    description="Simple procrastination monitor.",
    author='Franciszek Szczepan Wawrzak',
    author_email='frank@wawrzak.com',
    long_description=open('README.rst', 'r').read(),
    py_modules=[],
	scripts=['crastmon', 'crastmond'],
    url='http://github.com/fsw/crastmon/',
    license='BSD',
    install_requires=[
    ]
)
